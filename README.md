# Outlook Email Subject Updater

A tool to allow you to update the subject of received e-mails in Office 365 (and Outlook.com).

## Usage

Access the tool [https://tickett.gitlab.io/outlook-email-subject-updater/](https://tickett.gitlab.io/outlook-email-subject-updater/)
or [run locally](#run-locally).

- Select **Sign In**.
- (Optionally) enter a mailbox (leave the default `me` for your own mailbox,
  or user the format `users/someone@domain.com` for a shared mailbox).
- Select **Fetch Emails**.
- Select an e-mail.
- Enter the new subject in the prompt.
- Select **OK**.

## Run Locally

```shell
npm install
nppm run dev
```

Then access [http://localhost:5173/](http://localhost:5173/).
